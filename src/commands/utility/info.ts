import { CommandCategory, create_command, pluralize } from "#lib";
import dayjs from "dayjs";

export default create_command({
    names: ["info", "stats"],
    description: "Shows information about Mixtape.",
    category: CommandCategory.Utility,
    execute: ctx => {
        const description = [
            "Hello, I'm Mixtape! The perfect way to play your favorite tunes.",
            "[Discord Server](https://dimensional.fun/discord) • [Revolt Server](https://app.revolt.chat/invite/w41bYY96)\n",
            "### Stats",
            `**• Player Count:** ${pluralize("player", ctx.mixtape.players.size)}`,
            `**• Server Count:** ${pluralize("server", ctx.mixtape.servers.size)}`,
            `**• Uptime:** ${dayjs.duration(process.uptime() * 1000).format("HH:mm:ss")}\n`,
            "### Resource Usage",
            `**• Memory Usage:** ${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)}MiB`,
        ].join("\n");

        ctx.reply_primary(description);
    },
});
