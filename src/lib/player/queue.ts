export enum QueueType {
    /**
     * Increments the index.
     */
    Incremental,
    /**
     * 
     */
    Circular,
    /**
     * Opposite of {@link Incremental}, instead of incrementing the index, it stays the same.
     */
    Paused
}

export class Queue {
    constructor (readonly channelId: string) {
    }
}
